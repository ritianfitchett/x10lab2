//
//  Rasterizer.java
//
//  Created by Joe Geigel on 1/21/10.
//  Copyright 2010 Rochester Institute of Technology. All rights reserved.
//
//  Contributor:  Ian Fitchett
//                Wrote the drawPolygon function.
//

///
// 
// This is a class that performs rasterization algorithms
//
///

import java.util.*;

public class Rasterizer {
    
    ///
    // number of scanlines
    ///

    int n_scanlines;
    
    ///
    // Constructor
    //
    // @param n - number of scanlines
    //
    ///

    Rasterizer (int n)
    {
        n_scanlines = n;
    }
    
    ///
    // Draw a filled polygon in the simpleCanvas C.
    //
    // The polygon has n distinct vertices. The 
    // coordinates of the vertices making up the polygon are stored in the 
    // x and y arrays.  The ith vertex will have coordinate  (x[i], y[i])
    //
    // You are to add the implementation here using only calls
    // to C.setPixel()
    ///

    public void drawPolygon(int n, int x[], int y[], simpleCanvas C)
    {
        //Set the needed initializers.
        int ymax = 0;
        int ymin = 100000000;
        EdgeTable ET = new EdgeTable();
        for(int i = 0; i < n; i++){
            if(y[i] > ymax) ymax = y[i];
            if(y[i] < ymin) ymin = y[i];
        }
        ET.initET(ymin,ymax);

        //populate the edge table, assumes the points
        // are given in order. Start/end is irrelevant
        for(int i = 0; i < n; i++){
            Bucket temp = new Bucket();
            if(i < n-1){
                if(x[i] < x[i+1]) temp.initBucket(x[i], y[i], x[i+1], y[i+1]);
                else temp.initBucket(x[i+1], y[i+1], x[i], y[i]);
                ET.addBucket(Math.min(y[i],y[i+1]), temp);
            } else {
                if(x[0] < x[i]) temp.initBucket(x[0], y[0], x[i], y[i]);
                else temp.initBucket(x[i], y[i], x[0], y[0]);
                ET.addBucket(Math.min(y[i],y[0]), temp);
            }

        }

//        Debugging
//        ET.toConsole();

        //This loop has several responsibilities.
        // Each iteration checks a scanline. At each
        // scanline the program will put any associated
        // buckets into the AEL and remove any buckets
        // which have reached their ymax. Then, For each
        // pair of buckets in the AEL, it files the pixels
        // using odd parity.
        for(int i = 0; i < ET.yRange().length; i++){
            yPointer row = ET.yRange()[i];
            Bucket temp = row.next();
            if(temp != null) ET.putAEL(temp);
            //from here assume that AEL hass all necessary buckets
            row.killRow();
            temp = ET.AEL();
            while(temp != null){
                if(temp.ymax <= row.getY()) ET.delAEL(temp);
                temp = temp.next;
            }
//            Debugging
//            System.out.println(row.getY());
//            ET.printAEL();
            temp = ET.AEL();
            // this loop assumes even number of edges in list
            // This sets the piels of a single scanline and then
            // uses the bucket.update() function to increment dx and sum.
            while(temp != null){
                for(int j = temp.x; j <= temp.next.x; j++){
                    C.setPixel(j, row.getY());
                }
                temp.update();
                temp.next.update();
                temp = temp.next;
                temp = temp.next;
            }
        }
    }
}
