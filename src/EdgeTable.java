/**
 * Created by Ian on 9/29/2015.
 * File:    EdgeTable.java
 * Purpose: Define the structure of the edge table,
 *          a crucial structure for organization in
 *          the polygon-filling algorithm. Almost
 *          all of the work the program does is found
 *          here.
 */
public class EdgeTable {
    private yPointer[] yRange;
    private Bucket ActiveEdgeList;

    //yRange Getter
    public yPointer[] yRange(){
        return this.yRange;
    }

    //AEL Getter
    public Bucket AEL(){
        return ActiveEdgeList;
    }

    //initialize Edge Table with proper range,
    // no buckets, and an empty AEL
    public void initET(int start, int end){
        this.yRange = new yPointer[end-start+1];
        for(int i = 0; i < this.yRange.length; i++){
            this.yRange[i] = new yPointer();
            this.yRange[i].initYPointer(start+i);
        }
        ActiveEdgeList = null;
    }

    //Adds a bucket to y value of EdgeTable
    // Note that these y values do not coincide
    // with their indices.
    public void addBucket(int y,Bucket bucket){
        for(int i = 0; i < this.yRange.length; i++){
            if(this.yRange[i].getY() == y){
                if(this.yRange[i].next() == null) this.yRange[i].setPointer(bucket);
                else{
                    Bucket temp = this.yRange[i].next();
                    while(temp.next != null) temp = temp.next;
                    temp.setNext(bucket);
                }
                return;
            }
        }
    }

    //print just the AEL
    public void printAEL(){
        System.out.println(("AEL:"));
        if(ActiveEdgeList == null){
            return;
        }
        Bucket temp = ActiveEdgeList;
        while(temp.next != null){
            System.out.print(temp + ", ");
            temp = temp.next;
        }
        System.out.print(temp + "\n");
    }

    //prints the EdgeTable to console in a readable format.
    public void toConsole(){
        System.out.println("Edge Table:");
        for(int i = 0; i < this.yRange.length; i++){
            System.out.print(this.yRange[i].getY() + ": ");
            if(!this.yRange[i].hasNext()) System.out.println("Null");
            else{
                Bucket temp = this.yRange[i].next();
                System.out.print(temp + " ");
                while(temp != null && temp.next != null){
                    temp = temp.next;
                    System.out.print(temp + " ");
                }
                System.out.println();
            }
        }
        //now print the AEL
        printAEL();
    }

    //Put a single bucket into the Active Edge List
    public void putAEL(Bucket bucket){
        Bucket cur = this.ActiveEdgeList;
        if(cur == null) this.ActiveEdgeList = bucket;
        else{
            while(cur.next != null) {
                cur = cur.next;
            }
            cur.setNext(bucket);
        }
        sortAEL();
    }

    // uses quicksort helper algorithm that was
    // taken and modified by:
    // http://java2novice.com/java-sorting-algorithms/quick-sort/
    // Since the putAEL() function is naive, this
    // function is used to make sure the buckets are
    // in proper order after they've been put into the ael
    public void sortAEL(){
        //First put all the buckets in AEL into array
        Bucket[] sort = new Bucket[10];
        Bucket cur = this.ActiveEdgeList;
        int count = 0;
        for(;cur != null; count ++){
            sort[count] = cur;
            cur = cur.next;
        }
        // Then sort the array
        quickSort(sort, 0, count-1);
        //Now setNext() of every bucket to the next one in the array.
        for(int i = 0; i < count; i++){
            if(i < sort.length-1) sort[i].setNext(sort[i+1]);
            else sort[i].setNext(null);
        }
        //save changes
        this.ActiveEdgeList = sort[0];
    }

    //quicksort helper algorithm that was
    // taken and modified by:
    // http://java2novice.com/java-sorting-algorithms/quick-sort/
    private void quickSort(Bucket[] array, int lowerIndex, int higherIndex) {

        int i = lowerIndex;
        int j = higherIndex;
        // calculate pivot number, I am taking pivot as middle index number
        int pivot = array[lowerIndex +(higherIndex-lowerIndex)/2].x;
        // Divide into two arrays
        while (i <= j) {
            /**
             * In each iteration, we will identify a number from left side which
             * is greater then the pivot value, and also we will identify a number
             * from right side which is less then the pivot value. Once the search
             * is done, then we exchange both numbers.
             */
            while (array[i].x < pivot) {
                i++;
            }
            while (array[j].x > pivot) {
                j--;
            }
            if (i <= j) {
                exchangeNumbers(array, i, j);
                //move index to next position on both sides
                i++;
                j--;
            }
        }
        // call quickSort() method recursively
        if (lowerIndex < j)
            quickSort(array, lowerIndex, j);
        if (i < higherIndex)
            quickSort(array, i, higherIndex);
    }

    //quicksort helper algorithm that was
    // taken and modified by:
    // http://java2novice.com/java-sorting-algorithms/quick-sort/
    // Modified to properly use slope as a tie-breaker
    private void exchangeNumbers(Bucket[] array, int i, int j) {
        Bucket temp = array[i];
        if(array[i].x == array[j].x) {
            //if lines go in same direction and
            //if array[i] is steeper than array[j]
            //then don't swap
            if(array[i].dy < 0 && array[j].dy < 0 ||
                    array[i].dy > 0 && array[j].dy > 0){
                if((double)array[i].dy/array[i].dx > (double)array[j].dy/array[j].dx) return;
            //if lines go in opposite directions
            } else if(array[i].dy < array[j].dy) {
                return;
            }
        }
        array[i] = array[j];
        array[j] = temp;
    }

    //deletes a bucket from the AEL
    public void delAEL(Bucket bucket){
        Bucket temp = this.ActiveEdgeList;
        Bucket prev = null;
        while(temp != null){
            if(temp.equals(bucket)) {
                if (prev == null) this.ActiveEdgeList = temp.next;
                else prev.setNext(temp.next);
            }
            prev = temp;
            temp = temp.next;
        }
    }

}